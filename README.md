# ubi
Structure magic from TCDB sequences

## Summary

`Deuterocol1.py`: Protocol1 ortholog that fetches PDBs given TCDB identifiers to any depth (e.g. 1.A, 2.A.1, 9.B.104.1) and assigns transmembrane helices based on PDBTM data and STRIDE secondary-structure assignment.

 - DERP: Determine Egregious Rods in Proteins: Integrates PDBTM TMS assignments with STRIDE helix assignments using basic geometry. 
May be made into a standalone in the future.

`Deuterocol2.py`: Protocol2 ortholog that cuts PDBs from Deuterocol1 into bundles, and attempts structure alignment using superpose. 
Automatically summarizes data using quomodo.

`quomodo.py`: hvordan ortholog that generates stunning hypertext reports of Deuterocol2 results, including colored JSmol visualizations. 
Automatically called by Deuterocol2, without which the data would probably be somewhat incomprehensible. 
Not standalone (yet).

Interactive visualizations in the reports quomodo produces require JSmol, which can be downloaded from the [Jmol repository](http://jmol.sourceforge.net/). 
Very early testing suggests that this may not work on Chrome for reasons beyond the developer's current level of understanding.

## Dependencies

1. **_blast+ 2.4.0 to 2.6.0_**
Other versions of blast may require minor adaptations.
Visit the [download site](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download).

1. **_TCDB protein database_**
Given that blastdbcmd runs locally, the TCDB database must be available locally through the environment variable _$BLASTDB_.
If possible, use [extractFamily.pl](https://github.com/SaierLaboratory/TCDBtools/blob/master/scripts/extractFamily.pl) to download the TCDB BLAST database: 
```bash
extractFamily.pl -i all -f blast
```
Otherwise, you can manually download [all TCDB sequences](http://www.tcdb.org/public/tcdb) from the TCDB and run ```makeblastdb -in tcdb.fasta -out tcdb -dbtype prot```, but this script was not tested with such manually downloaded BLAST databases.

1. **_PDBTM database_**
Visit the [official website](http://pdbtm.enzim.hu/).
(Insert instructions on using dbtool to install the database and fix some XML)

1. **_Python 2.7+_**
Visit the [official website](https://www.python.org/).
This program was not tested with more recent versions of Python but was implemented with some measure of forward compatibility.

1. **_Biopython 1.70+_**
Visit the [official website](http://biopython.org/).

