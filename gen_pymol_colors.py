#!/usr/bin/env python

from __future__ import division, print_function

def colorize(iterable, start_hue=0, end_hue=240, sat=0.7, val=1.):
	step = (end_hue-start_hue)//(len(iterable)-1)
	for i, h in enumerate(range(start_hue, end_hue+step, step)):
		c = val * sat
		hp = (h/60) % 6
		x = c * (1 - abs(hp % 2 - 1))
		if 0 <= hp <= 1: r, g, b = c, x, 0
		elif 1 <= hp <= 2: r, g, b = x, c, 0
		elif 2 <= hp <= 3: r, g, b = 0, c, x
		elif 3 <= hp <= 4: r, g, b = 0, x, c
		elif 4 <= hp <= 5: r, g, b = x, 0, c
		elif 5 <= hp <= 6: r, g, b = c, 0, x
		else: r, g, b = 0, 0, 0
		m = val - c
		r += m
		g += m
		b += m
		
		r *= 255
		g *= 255
		b *= 255

		try: 
			print('color 0x%02.x%02.x%02.x, i. %d-%d' % (r, g, b, iterable[i][0], iterable[i][1]))
			print('select tms%d, i. %d-%d' % (i+1, iterable[i][0], iterable[i][1]))
		except IndexError: pass

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser()

	parser.add_argument('derpfile')

	args = parser.parse_args()

	ranges = []
	with open(args.derpfile) as f:
		for l in f: 
			if l.strip():
				ranges.append([int(x) for x in l.strip().split()[1:3]])
			else: pass
	colorize(ranges)
