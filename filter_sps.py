#!/usr/bin/env python2

from __future__ import print_function
import argparse, json, sys, time, os

import yaml

VERBOSITY = 1

def info(*substrs): 
	print('[INFO]: ', end='', file=sys.stderr)
	print(*substrs, file=sys.stderr)

def warn(*substrs): 
	print('[WARNING]: ', end='', file=sys.stderr)
	print(*substrs, file=sys.stderr)

def main(*infiles, **kwargs):
	try: rmsd_max = float(kwargs['rmsd_max'])
	except KeyError: rmsd_max = 5.
	except TypeError: rmsd_max = None

	try: length_min = int(kwargs['length_min'])
	except KeyError: length_min = 60
	except TypeError: length_min = None

	try: deficit_max = int(kwargs['deficit_max'])
	except KeyError: deficit_max = 15
	except TypeError: deficit_max = None

	try: out = kwargs['out']
	except KeyError: out = '/dev/stdout'

	try: 
		tms_min = kwargs['tms_min']
		if tms_min is not None:
			if 'p1d' in kwargs: p1d = kwargs['p1d']
			else: raise TypeError('Expected argument p1d')
	except KeyError: 
		tms_min = None
	if tms_min is None: p1d = None

	try: tms_threshold = kwargs['tms_threshold']
	except KeyError: tms_threshold = 10

	try: 
		purge_reciprocals = kwargs['purge_reciprocals']
		if purge_reciprocals:
			if 'p2d' in kwargs: p2d = kwargs['p2d']
			else: raise TypeError('Expected argument p2d')

			with open('{0}/deuterocol2.yaml'.format(p2d)) as f: 
				d2data = yaml.load_all(f).next()
				b2a = {}
				for pdbc in d2data['b2a']: b2a[pdbc] = sorted(d2data['b2a'][pdbc])[0]
				#print(d2data)
				#for section in d2data: 
				#	try: print(section, section.tag, section.value)
				#	except AttributeError: pass

	except KeyError: 
		purge_reciprocals = None
		p2d = None
	if purge_reciprocals is None: p2d = None

#	if VERBOSITY:
#		info('rmsd_max = {0}'.format(rmsd_max))
#		info('length_min = {0}'.format(length_min))
#		info('deficit_max = {0}'.format(deficit_max))
#		info('tms_min = {0}'.format(tms_min))
#		info('tms_threshold = {0}'.format(tms_threshold))
#		info('p1d = {0}'.format(p1d))
#		info('p2d = {0}'.format(p2d))
#		info('purge_reciprocals = {0}'.format(purge_reciprocals))

	with open(out, 'w') as fout:
		for fn in infiles:
			with open(fn) as f:
				n = 0
				last_t = time.time()
				for l in f:
					n += 1
					if VERBOSITY and not (n % 10000): 
						info('%d lines parsed (%0.2f)' % (n, time.time() - last_t))
						last_t = time.time()
					if not l.strip(): continue
					elif l.lstrip().startswith('#'): continue
					else:
						sl = l.split('\t')
						try: obj = json.loads(sl[1])
						except IndexError: 
							if VERBOSITY > 1: warn('No alignment found for %s' % l.rstrip())
							continue
						except ValueError:
							if VERBOSITY > 1: warn('No alignment found for %s' % l.rstrip())
							continue
						if (rmsd_max is not None) and (obj['rmsd'] > rmsd_max): 
							if VERBOSITY > 1: info('Rejected: High RMSD')
							continue
						elif (length_min is not None) and (obj['length'] < length_min): 
							if VERBOSITY > 1: info('Rejected: Low length')
							continue
						elif (deficit_max is not None) and ((len(obj['qpresent']) - len(obj['qaligned'])) > deficit_max): 
							if VERBOSITY > 1: info('Rejected: High deficit in q')
							continue
						elif (deficit_max is not None) and ((len(obj['tpresent']) - len(obj['taligned'])) > deficit_max): 
							if VERBOSITY > 1: info('Rejected: High deficit in t')
							continue
						elif (tms_min is not None) and tms_min and not has_n_tmss(obj, tms_min, tms_threshold, p1d): 
							if VERBOSITY > 1: info('Rejected: Not enough TMSs')
							continue
						elif purge_reciprocals and not in_order(obj, b2a): 
							if VERBOSITY > 1: info('Rejected: In reciprocal order')
							continue
						else: 
							fout.write(l)
def in_order(obj, b2a):
	qpdbc = os.path.splitext(os.path.basename(obj['query']))[0]
	tpdbc = os.path.splitext(os.path.basename(obj['target']))[0]
	if b2a[qpdbc] <= b2a[tpdbc]: return True
	else: return False

def has_n_tmss(obj, tms_min, threshold, p1d):
	qpdbc = os.path.splitext(os.path.basename(obj['query']))[0]
	tpdbc = os.path.splitext(os.path.basename(obj['target']))[0]
	#if os.path.isfile('{p1d}/derp/{qpdbc}.derp'
	qaligned = set(obj['qaligned'])
	taligned = set(obj['taligned'])

	qcount = 0
	tcount = 0
	with open('{0}/derp/{1}.derp'.format(p1d, qpdbc)) as f:
		for l in f: 
			if not l.strip(): continue
			sl = l.split()

			cov = qaligned.intersection(set(range(int(sl[1]), int(sl[2])+1)))
			if len(cov) >= threshold: qcount += 1

	with open('{0}/derp/{1}.derp'.format(p1d, tpdbc)) as f:
		for l in f: 
			if not l.strip(): continue
			sl = l.split()

			cov = taligned.intersection(set(range(int(sl[1]), int(sl[2])+1)))
			if len(cov) >= threshold: tcount += 1

	if min(qcount, tcount) >= tms_min: return True
	else: return False

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--rmsd-max', type=float)
	parser.add_argument('--length-min', type=int)
	parser.add_argument('--deficit-max', type=int, help='maximum residues in *present but not in *aligned')

	parser.add_argument('--p1d', help='Deuterocol1 directory. Only required if counting TMSs')
	parser.add_argument('--tms-min', type=int, help='require this many TMSs')
	parser.add_argument('--tms-threshold', type=int, default=10, help='require this many aligned residues to count a TMS')

	parser.add_argument('--p2d', help='Deuterocol2 directory. Only required if purging reciprocals')
	parser.add_argument('--purge-reciprocals', action='store_true', help='retain only alignments where the first family is less than the second')
	parser.add_argument('-o', '--outfile', default='/dev/stdout')
	parser.add_argument('infile', nargs='+')

	parser.add_argument('-v', action='store_true')

	args = parser.parse_args()

	if args.tms_min is not None and args.p1d is None:
		parser.print_help()
		exit()

	if args.purge_reciprocals and args.p2d is None:
		parser.print_help()
		exit()

	if args.v: VERBOSITY = 2


	main(*args.infile, \
		out=args.outfile, \
		rmsd_max=args.rmsd_max, \
		length_min=args.length_min, \
		deficit_max=args.deficit_max, \
		p1d=args.p1d, \
		tms_min=args.tms_min, \
		tms_threshold=args.tms_threshold, \
		p2d=args.p2d, \
		purge_reciprocals=args.purge_reciprocals \
	)
