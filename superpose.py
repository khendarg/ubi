#!/usr/bin/env python
from __future__ import print_function, division
import json, os, sys
import Bio.PDB

try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

def error(*lines):
	for line in lines: print(line, file=sys.stderr)

	exit()

def load_pdb(fn=None, prefix=None):
	if prefix == None: fn = fn
	else: fn = prefix + '/' + os.path.basename(fn)
	pdb = PDB(fn)

	return pdb

class Superpose(object):
	'''
Contains the data present in superpose output
	'''
	def __init__(self, f=None, fn=None, format='superpose', name=None):
		'''
Initializes fields
		'''
		#TODO: decouple parsing from stuff, whoops
		self.f = f
		if format != 'empty':
			if fn is not None: self.fn = fn
			else: 
				try: self.fn = f.name
				except AttributeError: 
					if name is None: raise ValueError('name needs to be specified')
					else: self.fn  = name
					

		self.qfull = 'None'
		self.tfull = 'None'

		self.qfam = 'Unassigned'
		self.tfam = 'Unassigned'

		self.qaligned = []
		self.taligned = []
		self.qpresent = []
		self.tpresent = []

		self.interactive = True

		self.queryfn = 'None'
		self.qpdbc = 'None'
		self.queryc = ''
		self.targetfn = 'None'
		self.tpdbc = 'None'
		self.targetc = ''
		self.quality = -1
		self.rmsd = -1
		self.length = -1
		self.matrixstr = ''

		self.helstr = 'None'
		self.tmsstr = 'None'

		self.reason = ''

		self.tmss = []

		if format == 'superpose': self.parse()
		elif format == 'json': self.load_json()
		elif format == 'empty': return

	def load_pdbs(prefix=None):
		if prefix == None: prefix = '.'
		self.query = PDB(prefix + '/' + self.qpdbc + '.pdb')
		self.target = PDB(prefix + '/' + self.tpdbc + '.pdb')

	def worst_resolution(self): return max(self.query.resolution, self.target.resolution)

	def __str__(self):
		out = ''
		#out += '\t' + os.path.basename(self.queryfn[:self.queryfn.find('.')])
		#out += '\t' + os.path.basename(self.targetfn[:self.targetfn.find('.')])
		out += '\t' + self.qfull
		out += '\t' + self.tfull
		#out += '\t' + self.queryfn
		#out += '\t' + self.targetfn
		#qpres = self.qpresent[0]
		#out += '\t%s' % (qpres[1]-qpres[0])
		#tpres = self.tpresent[0]
		#out += '\t%s' % (tpres[1]-tpres[0])
		out += '\t%d' % len(self.qpresent)
		out += '\t%d' % len(self.tpresent)

		#reftms
		out += '\t%d' % len(self.query.tmss)
		#movtms
		out += '\t%d' % len(self.target.tmss)

		out += '\t%0.2f' % self.rmsd
		out += '\t%d' % self.length

		if len(self.queryc) < len(self.targetc): cov = self.length/len(self.queryc)
		else: cov = self.length/len(self.targetc)
		out += '\t%0.2f' % cov

		out += '\tsuperpose'

		#TMSs
		out += '\t' + self.tmsstr
		out += '\t' + self.helstr

		out += '\t' + self.reason

		return out.lstrip()

	def is_chimeric(self):
		if self.query.is_chimeric(self.qpdbc[-1]) or self.target.is_chimeric(self.tpdbc[-1]): return True
		else: return False

	def list2ranges(self, l):
		'''
Converts a list into a list of closed intervals
		'''
		if not l: return []
		ranges = [(l[0], l[0])]
		for i in range(1, len(l)):
			if l[i] == ranges[-1][1]+1: ranges[-1] = (ranges[-1][0], l[i])
			else: ranges.append((l[i], l[i]))
		return ranges

	def parse(self):
		'''
Parses superpose output
		'''
		matrixrec = 0
		recording = 0
		alnposition = 0
		for l in self.f:

			#pick up query and target filenames
			if self.queryfn == 'None' and l.startswith(' PDB file '): 
				self.queryfn = l.strip().split()[2]
				self.qpdbc = os.path.basename(self.queryfn)[:6]
			elif self.targetfn == 'None' and l.startswith(' PDB file '): 
				self.targetfn = l.strip().split()[2]
				self.tpdbc = os.path.basename(self.targetfn)[:6]

			#pick up the matrix
			elif not self.matrixstr and l.startswith('        Rx'): matrixrec = 1
			elif matrixrec and not l.strip(): matrixrec = 0
			elif matrixrec: self.matrixstr += '[' + l.strip().replace('  ', ' ') + '] '

			#??? something about a failure condition
			elif not self.qpresent and l.startswith(' ...'): 
				cid = l.strip().split()[-1]
				cid = cid[cid.find('/')+1:-1]#.split('-')
				if cid.startswith('-'): 
					cid = cid[1:].split('-')
					cid[0] = '-' + cid[0]
				else: cid = cid.split('-')
				self.qpresent = list(range(int(cid[0]), int(cid[1])))
				#except ValueError: 
				#	error(l, cid, self.fn)
			elif not self.tpresent and l.startswith(' ...'):
				cid = l.strip().split()[-1]
				cid = cid[cid.find('/')+1:-1]

				if cid.startswith('-'): 
					cid = cid[1:].split('-')
					cid[0] = '-' + cid[0]
				else: cid = cid.split('-')

				self.tpresent = list(range(int(cid[0]), int(cid[1])))
				#except ValueError: print(cid)

			#more weird failure conditions
			elif l.startswith(' *** Selection string'): raise ZeroDivisionError
				

			#important stats
			elif l.startswith('   quality'): self.quality = float(l[15:21].strip())
			elif l.startswith('     r.m.s'): self.rmsd = float(l[14:21].strip())
			elif l.startswith('      Nali'): self.length = int(l[15:21].strip())
			elif l.startswith(' |-------------+'): recording = 1
			elif l.startswith(' `-------------'): recording = 0

			#aligned sequence
			elif recording:
				#if l[10:14].strip(): self.qpresent.append(int(l[10:14].strip()))
				#if l[35:39].strip(): self.tpresent.append(int(l[35:39].strip()))
				if l[18:23].strip():
					#self.qaligned.append(self.qpresent[-1])
					#self.taligned.append(self.tpresent[-1])
					self.qaligned.append(int(l[10:14].strip()))
					try: self.taligned.append(int(l[35:39].strip()))
					except ValueError: self.taligned.append(int(l[40:44].strip()))
				alnposition += 1

		#reformatting the matrix for Jmol
		if self.matrixstr: 
			self.matrixstr += '[0.0 0.0 0.0 1.0]'
			self.matrixstr = '[' + self.matrixstr.replace('  ', ' ') + ']'

		#counting aligned residues
		self.qaligned# = self.list2ranges(self.qaligned)
		self.taligned# = self.list2ranges(self.taligned)
		self.qpresent# = self.list2ranges(self.qpresent)
		self.tpresent# = self.list2ranges(self.tpresent)

		#skip displaying a Jmol if one or more PDBs failed to load
		if not self.tpresent: self.interactive = False
		elif not self.qpresent: self.interactive = False

	def compute_aligned_tmss(self, threshold=10):
		qtms = []
		for t in self.query.tmss: qtms.append(0)
		ttms = []
		for t in self.target.tmss: ttms.append(0)

		for i in self.qaligned:
			for t, span in enumerate(self.query.tmss):
				if span[0] <= i <= span[1]: qtms[t] += 1

		for i in self.taligned:
			for t, span in enumerate(self.target.tmss):
				if span[0] <= i <= span[1]: ttms[t] += 1


		#get ideal TMS ranges

		#get ideal start for query
		qstart, bestdiff = None, None
		for i, span in enumerate(self.query.tmss):
			if bestdiff == None or (abs(span[0]-min(self.qpresent)) < bestdiff): 
				qstart = i
				bestdiff = abs(span[0]-min(self.qpresent))
		#get ideal end for query
		qend, bestdiff = None, None
		for i, span in enumerate(self.query.tmss):
			if bestdiff == None or (abs(span[0]-max(self.qpresent)) < bestdiff): 
				qend = i
				bestdiff = abs(span[0]-max(self.qpresent))

		#get ideal start for target
		tstart, bestdiff = None, None
		for i, span in enumerate(self.target.tmss):
			if bestdiff == None or (abs(span[0]-min(self.tpresent)) < bestdiff): 
				tstart = i
				bestdiff = abs(span[0]-min(self.tpresent))
		#get ideal end for query
		tend, bestdiff = None, None
		for i, span in enumerate(self.target.tmss):
			if bestdiff == None or (abs(span[0]-max(self.tpresent)) < bestdiff): 
				tend = i
				bestdiff = abs(span[0]-max(self.tpresent))

		qtmss = []
		for i in range(qstart, qend+1):
			if qtms[i] >= threshold: qtmss.append(i)
			else: qtmss.append(None)

		ttmss = []
		for i in range(tstart, tend+1):
			if ttms[i] >= threshold: ttmss.append(i)
			else: ttmss.append(None)

		tmsstr = ''
		helstr = ''
		for x in zip(qtmss, ttmss):
			notms = False
			pair = ''
			if x[0] is None: 
				pair += '_'
				notms = True
			else: pair += str(x[0]+1)

			pair += '-'

			if x[1] is None: 
				pair += '_'
				notms = True
			else: pair += str(x[1]+1)

			pair += ','
			if not notms: tmsstr += pair
			helstr += pair
		tmsstr = tmsstr[:-1]
		helstr = helstr[:-1]
		self.tmsstr = tmsstr
		self.helstr = helstr

	def __lt__(self, other):
		if self.rmsd == -1 and other.rmsd != -1: return False
		elif self.rmsd != -1 and other.rmsd == -1: return True
		#elif self.rmsd == -1 and other.rmsd == -1:

		if self.length > other.length: return True
		elif self.length == other.length: return False
		elif self.length == other.length: return (self.queryfn+self.targetfn < other.queryfn+other.targetfn)

		#for i, n in enumerate(qtms):

	def __lt__(self, other):
		if self.rmsd == -1 and other.rmsd != -1: return False
		elif self.rmsd != -1 and other.rmsd == -1: return True
		#elif self.rmsd == -1 and other.rmsd == -1:

		if self.length > other.length: return True
		elif self.length == other.length: return False
		elif self.length == other.length: return (self.queryfn+self.targetfn < other.queryfn+other.targetfn)
		#else:

	def dump_json(self):
		outdict = {}

		outdict.update({'query':self.queryfn})
		outdict.update({'target':self.targetfn})
		outdict.update({'matrixstr':self.matrixstr})
		outdict.update({'quality':self.quality})
		outdict.update({'rmsd':self.rmsd})
		outdict.update({'length':self.length})
		outdict.update({'qpresent':self.qpresent})
		outdict.update({'tpresent':self.tpresent})
		outdict.update({'qaligned':self.qaligned})
		outdict.update({'taligned':self.taligned})

		out = json.dumps(outdict)
		
		return out

	def load_json(self):
		jsonstr = ''
		for l in self.f: jsonstr += l
		obj = json.loads(jsonstr)

		if 'query' in obj: 
			self.queryfn = obj['query']
			self.qpdbc = os.path.basename(self.queryfn)[:6]
		if 'target' in obj: 
			self.targetfn = obj['target']
			self.tpdbc = os.path.basename(self.targetfn)[:6]
		if 'matrixstr' in obj: self.matrixstr = obj['matrixstr']
		if 'quality' in obj: self.quality = obj['quality']
		if 'rmsd' in obj: self.rmsd = obj['rmsd']
		if 'length' in obj: self.length = obj['length']
		if 'qpresent' in obj: self.qpresent = obj['qpresent']
		if 'tpresent' in obj: self.tpresent = obj['tpresent']
		if 'qaligned' in obj: self.qaligned = obj['qaligned']
		if 'taligned' in obj: self.taligned = obj['taligned']

class PDB(object):
	'''
Contains relevant data tied to PDBs
	'''
	def __init__(self, pdbfn):
		self.fn = pdbfn
		p = Bio.PDB.PDBParser()
		self.structure = p.get_structure(os.path.basename(self.fn), self.fn)
		self.header = p.get_header()
		self.pdbc = os.path.basename(pdbfn)[:6]
		self.chains = []
		for chain in self.structure.get_chains():
			self.chains.append(Chain(chain))
		self.tmss = []

		self.blast = {}

		self.resolution = self.header['resolution']
		if self.resolution is None: self.resolution = -1

	def is_chimeric(self, chain='A'):
		chainlink = {}
		with open(self.fn) as f:
			for l in f:
				if l.startswith('DBREF'):
					try: chainlink[l[12]].append(l[33:41].strip())
					except KeyError: chainlink[l[12]] = [l[33:41].strip()]
		try: 
			if len(chainlink[chain]) <= 1: return False
			else: return True
		except KeyError: return True
		#taxids = [self.header['source'][cnum]['organism_taxid'].split(', ') for cnum in self.header['source']]
		#n = 0
		#for x in taxids: n += len(x)

		##print(self.pdbc, n, len(self.chains))
		#try: 
		#	fragments = [self.header['compound'][x]['fragment'] for x in self.header['compound']]
		#	for frag in fragments:
		#		if ',' in frag: return True
		#	return False
		#except KeyError: return False

	def load_derp(self, fn):
		with open(fn) as f:
			for l in f:
				if not l.strip(): continue
				elif l.strip().startswith('#'): continue
				else: 
					sl = l.strip().split()
					self.tmss.append((int(sl[-2]), int(sl[-1])))

class Chain(object):
	'''
Contains relevant data tied to PDB chains
	'''

	def __init__(self, chain):
		self.chain = chain
		self.id = chain.id

		n = 0
		for residue in self.chain:
			try:
				CODE[residue.get_resname()]
				n += 1
			except KeyError: continue
		self.length = n
		
	def __len__(self): return self.length

if __name__ == '__main__':
	#spfn = 'quomodotest/ubi_out/superpose/5er7_B_1-4_v_3jbr_E_1-4.superpose'
	#jsonfn = '5er7_B_1-4_v_3jbr_E_1-4.json'

	#with open(spfn) as f: 
	#	test = Superpose(f)
	##print(test.dump_json())

	##print(test)
	#with open(jsonfn) as f:
	#	testload = Superpose(f, format='json')
	#print(testload.dump_json())
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('-d', default=['.'], nargs='+', help='directory/ies to compress')
	parser.add_argument('-f', default=None, nargs='+', help='file(s) to compress')
	parser.add_argument('-o', default=None, help='target file')

	args = parser.parse_args()
	#TODO: improve this handling
	if not args.o: args.o = args.d[0]
	#if args.o != '.' and not os.path.isdir(args.o): os.mkdir(args.o)

	g = open('%s' % (args.o), 'w')
	for dirname in args.d: 
		for basename in os.listdir(dirname):
			if basename.endswith('superpose'):
				fn = '%s/%s' % (dirname, basename)

				with open(fn) as f:
					try: 
						sp = Superpose(f, format='superpose')
						g.write('%s\t%s\n' % (basename, sp.dump_json()))
					except ZeroDivisionError: 
						g.write('%s\t\n' % (basename))
