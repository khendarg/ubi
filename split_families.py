#!/usr/bin/env python2

from __future__ import print_function, division
import json, sys, os, yaml, re
import argparse

VERBOSITY = 1

def info(*things): 
	print('[INFO]', end=' ', file=sys.stderr)
	print(*things, file=sys.stderr)

def warn(*things): 
	print('[WARNING]', end=' ', file=sys.stderr)
	print(*things, file=sys.stderr)

class TCID(object):
	def __init__(self, tcstr):
		if tcstr.count('.') < 4: raise ValueError
		self.fields = re.split('[-\.]', tcstr)

	def __getitem__(self, key):
		out = ''
		if type(key) is int: return self.fields[key]
		elif type(key) is slice: 
			if key.start is None: a = 0
			else: a = key.start

			if key.step is None: b = 1
			else: b = key.step

			for i, component in enumerate(self.fields[key]):
				n = (a + i*b)
				if n == 0: pass
				elif i == 0: pass
				elif n == 5: out += '-'
				else: out += '.'
				out += component

		return out

class Buffer(object):
	def __init__(self, threshold=1000, outdir='new_directory'):
		self.outdir = outdir
		self.queues = {}
		self.threshold = threshold
		if not os.path.isdir(outdir): os.mkdir(outdir)

	def add(self, famtuple, data):
		try: self.queues[famtuple].append(data)
		except KeyError: 
			self.queues[famtuple] = [data]
			open('%s/%s_%s.tsv' % ((self.outdir, ) + famtuple), 'w')

		if not len(self.queues[famtuple]) % self.threshold:
			with open('%s/%s_%s.tsv' % ((self.outdir, ) + famtuple), 'a') as f:
				for l in self.queues[famtuple]: f.write(l + '\n')
			self.queues[famtuple] = []

	def flush(self):
		for famtuple in self.queues:
			with open('%s/%s_%s.tsv' % ((self.outdir, ) + famtuple), 'a') as f:
				for l in self.queues[famtuple]: f.write(l + '\n')
			self.queues[famtuple] = []

def main(*args, **kwargs): 
	try: depth = int(kwargs['depth'])
	except KeyError: depth = 3

	try: p2d = kwargs['p2d']
	except KeyError: p2d = '.'

	try: outdir = kwargs['outdir']
	except KeyError: outdir = 'split_sps'

	if not os.path.isdir(outdir): os.mkdir(outdir)

	with open('%s/deuterocol2.yaml' % (p2d)) as f:
		d2 = yaml.load(f)

	bfr = Buffer(outdir=outdir)

	n = 0
	for fn in args:
		with open(fn) as f:
			for l in f:
				n += 1
				if not l.strip(): continue
				elif l.lstrip().startswith('#'): continue
				else: 
					sl = l.rstrip().split('\t')
					if len(sl) < 2: continue
					obj = json.loads(sl[1])
					qpdbc = os.path.splitext(os.path.basename(obj['query']))[0]
					tpdbc = os.path.splitext(os.path.basename(obj['target']))[0]

					qfam = sorted(d2['b2a'][qpdbc])[0]
					tfam = sorted(d2['b2a'][tpdbc])[0]

					fams = tuple(sorted([TCID(qfam)[:depth], TCID(tfam)[:depth]]))

					bfr.add(fams, l.rstrip())
				if VERBOSITY and not (n % 10000): info(n, 'lines sorted')

	bfr.flush()

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-d', '--depth', default=3, type=int, help='TCID depth to split to {default:3}')
	parser.add_argument('-o', '--outdir')
	parser.add_argument('-p', '--p2d', default='.', help='Protocol2 directory')
	parser.add_argument('infile', nargs='+', help='Full->JSON table')

	args = parser.parse_args()

	main(*args.infile, depth=args.depth, p2d=args.p2d)
