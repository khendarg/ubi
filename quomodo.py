#!/usr/bin/env python2
#Qualitative Viewer Of More Outer-Dimensional Outputs
from __future__ import print_function, division, unicode_literals
import yaml, os, sys, shutil, json
import re
#TODO: stop using io when a str will do
import io
import Bio.PDB
import superpose
import argparse

try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

VERBOSITY = 1
DEBUG = 1
import time

def warn(*lines):
	for line in lines: print('[WARNING]: %s' % line, file=sys.stderr)

def info(*lines):
	for line in lines: print('[INFO]: %s' % line, file=sys.stderr)

def collapse(indices):
	ranges = []
	for i in indices:
		if not ranges: ranges.append([i, i])
		elif i == ranges[-1][-1] + 1: ranges[-1][-1] += 1
		else: ranges.append([i, i])
	return ranges

def is_alignment_good(alignment, lencutoff=50, tmsthreshold=10, bundlecov=0.8, mincov=0.8, worst_resolution=4., allow_chimeras=False, maxrmsd=5.0, d2data=None):
	if not alignment.qpresent: 
		alignment.reason = 'no residues in query bundle (%s vs %s)' % (alignment.qfull, alignment.tfull)
		return False
	elif not alignment.tpresent:
		alignment.reason = 'no residues in target bundle (%s vs %s)' % (alignment.qfull, alignment.tfull)
		return False
	elif len(alignment.qpresent) < len(alignment.tpresent): cov = alignment.length/len(alignment.qpresent) 
	else: cov = alignment.length/len(alignment.tpresent) 

	#test 1: Does the alignment involve enough residues?
	if alignment.length < lencutoff: 
		alignment.reason = 'too short'
		return False

	#test 2: Does the alignment cover at least 80% of the TMSs in a bundle?
	elif (d2data is not None) and (len(alignment.tmsstr.split(',')) < (bundlecov * d2data['bundle'])): 
		alignment.reason = 'not enough TMSs'
		return False

	#test 3: Does the alignment cover at least mincov of the shorter sequence?
	elif cov < mincov:
		alignment.reason = 'not enough coverage'
		return False

	#test 4: contiguous bundles
	#elif '_' in alignment.helstr and not (alignment.helstr.startswith('_') or alignment.helstr.endswith('_')):
	elif '_' in ''.join(alignment.helstr.split(',')[1:-1]):
		alignment.reason = 'discontiguous bundle'
		return False

	#test 5: no chimeras
	elif (not allow_chimeras) and alignment.is_chimeric():
		alignment.reason = 'chimera'
		return False

	#test 6: no poor-resolution
	elif alignment.worst_resolution() > worst_resolution:
		alignment.reason = 'poor resolution'
		return False

	#test 7: no multimodels? no computational models, at least

	#test 8: Does the alignment have a reasonable RMSD?
	elif alignment.rmsd > maxrmsd:
		alignment.reason = 'poor RMSD'
		return False

	#pretest: minimum length

	#final test: interpretation: check that repeat units make sense
	#this is on you, meatbags

	else: return True

class Quomodo(object):
	'''
A class containing the data needed to generate summaries
	'''
	def __init__(self, d1dict, d2dict, outdir='ubi_out', jsmoldir='jsmol', irfmode=False, outfile=None, outfmt='tsv'):
		'''
Prepares the output directory and includes inputs
		'''
		self.d1 = d1dict
		self.d2 = d2dict
		self.outdir = outdir
		self.jsmoldir = jsmoldir
		self.resolutions = {}
		self.irfmode = irfmode
		self.outfile = outfile
		self.outfmt = outfmt
		if not os.path.isdir(self.outdir): os.mkdir(self.outdir)

	def get_tms_coverages(self, aln, sp, threshold=0.5):
		'''
Checks that the number of residues in each TMS is at least **threshold**
		'''
		a = aln[:-10].split('_')
		qtms = []
		ttms = []
		with open('%s/derp/%s_%s.derp' % (self.d2['p1d'], a[0], a[1])) as f: 
			for l in f:
				sl = l.split()
				if not l.strip(): continue
				else: qtms.append((int(sl[1]), int(sl[2])))
		with open('%s/derp/%s_%s.derp' % (self.d2['p1d'], a[4], a[5])) as f: 
			for l in f:
				sl = l.split()
				if not l.strip(): continue
				else: ttms.append((int(sl[1]), int(sl[2])))
		qcov = []
		tcov = []
		for tms in qtms:
			qcov.append(0)
			for r in sp.qaligned:
				for i in range(r[0], r[1]+1):
					if tms[0] <= i <= tms[1]: qcov[-1] += 1
		for tms in ttms:
			tcov.append(0)
			for r in sp.taligned:
				for i in range(r[0], r[1]+1):
					if tms[0] <= i <= tms[1]: tcov[-1] += 1
		qt = []
		tt = []
		for i, x in enumerate(zip(qcov, qtms)): 
			#print(x[0]/(x[1][1]-x[1][0]+1))
			if x[0]/(x[1][1]-x[1][0]+1) >= 0.5: qt.append(i+1)
		for i, x in enumerate(zip(tcov, ttms)): 
			#print(x[0]/(x[1][1]-x[1][0]+1))
			if x[0]/(x[1][1]-x[1][0]+1) >= 0.5: tt.append(i+1)
		qstr = ''
		tstr = ''
		for i in qt: qstr += '%d-' % i
		if qstr: qstr = qstr[:-1]
		for i in tt: tstr += '%d-' % i
		if tstr: tstr = tstr[:-1]

		return qstr, tstr

	def get_max_pres(self, sp):
		'''
Returns the maximum align length for a given superposition
		'''
		q, t = 0, 0
		for r in sp.qpresent: q += r[1] - r[0] + 1
		for r in sp.tpresent: t += r[1] - r[0] + 1
		return max(q, t)

	def generate_highlight_table(self, f):
		'''
Generates the residue-highlighting
		'''
		recording = 0
		out = ''
		def nb(x): return x.replace(' ', '&nbsp;')
		for l in f:
			if l.startswith(' .---'): recording = 1
			elif recording:
				if l.startswith('$$'): recording = 0
				elif not l.strip(): continue
				elif l.startswith(' |---'): out += '<div class="scrollable"><table>'
				elif l.startswith(' `---'): out += '</table></div>'
				elif l.startswith(' |    Query'): pass
				elif l.startswith(' |'): 
					out += '<tr>'

					out += '<td class="monospace">'
					if l[10:14].strip(): 
						qindex = int(l[10:14].strip())
						out += nb(l[2:7])
						x = l[7:14]
						if x[4] != ' ': x = x[:3] + ' ' + x[4:]
						out += '<a class="monospace" href="#" onclick="Jmol.script(jmolApplet0, \'color {%d /1.1} \'+document.getElementById(\'paintcolor\').value);">%s</a>' % (qindex, x)
					out += '</td>'

					out += '<td class="monospace">'
					if l[17:25].strip(): out += nb(l[17:25])
					out += '</td>'

					out += '<td class="monospace">'
					if l[35:39].strip(): 
						tindex = int(l[35:39].strip())
						out += nb(l[27:32])
						x = l[32:39]
						if x[4] != ' ': x = x[:3] + ' ' + x[4:]
						out += '<a class="monospace" href="#" onclick="Jmol.script(jmolApplet0, \'color {%d /2.1} [x33FF33]\');">%s</a>' % (tindex, x)
					out += '</td>'
					out += '</tr>\n'
				else: out += l.replace('\n', '<br/>')
		return out

	def get_resolutions(self, allpdbs):
		'''
Obtains resolutions for PDBs

allpdbs: dict in 'pdbc':superpose.PDB format
		'''
		pdbset = set()
		for pdb in allpdbs: pdbset.add(pdb)

		for pdb in sorted(pdbset): pass

	def build_reports(self, lencutoff=50, tmsthreshold=10, bundlecov=0.8, mincov=0.8, outfile='summary.tsv', worst_resolution=4., allow_chimeras=False, nmin=0, nmax=1e99, clobber=True, maxrmsd=5.0):
		'''
Builds HTML/TSV reports

lencutoff: Minimum residues in alignment

tmsthreshold: Minimum residues required for a TMS to count

bundlecov: Minimum fraction of bundle size required in alignment

mincov: Minimum fraction of the shorter chain required
		'''

		if not os.path.isdir('%s/html' % self.outdir): os.mkdir('%s/html' % self.outdir)
		if not os.path.isdir('%s/html/pdbs' % self.outdir): os.mkdir('%s/html/pdbs' % self.outdir)
		if not os.path.isdir('%s/html/jsmol' % self.outdir): os.mkdir('%s/html/jsmol' % self.outdir)

		copypdbs = []

		#everything in p2dir/superpose
		spfns = os.listdir('%s/superpose' % self.d2['p2d'])
		spprefix = '%s/superpose' % self.d2['p2d']
		#stuff that is in p2dir but isn't relevant
		removeme = []
		#superposes

		#get rid of junk, i.e.
		if VERBOSITY: info('Dequeuing junk: %d files...' % len(spfns))
		for i, fn in enumerate(spfns):
			#hidden files
			if VERBOSITY and not (i % 1000): info('%d/%d reviewed for dequeuing' % (i, len(spfns)))
			if fn.startswith('.'): removeme.append(fn)
			#non-tsv files
			elif not fn.endswith('.tsv'): removeme.append(fn)

		if VERBOSITY: info('Done reviewing files. Dequeuing %d files...' % (len(removeme)))

		#now remove them from any summarization at all
		for fn in removeme: spfns.remove(fn)

		if VERBOSITY: info('Getting resolutions...')

		self.alngood = {}
		self.alnbad = {}

		self.alignments = {}
		for fam1 in self.d2['fams1']:
			self.alignments[fam1] = {}
			self.alngood[fam1] = {}
			self.alnbad[fam1] = {}
			for fam2 in self.d2['fams2']:
				self.alignments[fam1][fam2] = []
				self.alngood[fam1][fam2] = []
				self.alnbad[fam1][fam2] = []

		
		n = 0
		sps = []

		pdbdir = '%s/pdbs_loopless' % self.d2['p1d']
		pdbs = {}

		def to_pdbc(fn): return os.path.basename(fn)[:6]


		for fn in spfns:
			if VERBOSITY: info('Loading from %s' % fn)
			with open(spprefix + '/' + fn) as f:
				for l in f:
					n += 1
					if n > nmax: break
					elif n < nmin: continue
					sl = l.split('\t')
					spfn = os.path.basename(sl[0])
					
					spf = io.StringIO(sl[1])

					if not sl[1].strip(): continue

					sp = superpose.Superpose(spf, fn=spfn, format='json')
					sp.qfull, sp.tfull = tuple(spfn[:spfn.find('.')].split('_v_'))

					#if not sp.qpresent or not sp.tpresent: continue
					#print(self.alignments)
					if not sp.qaligned or not sp.taligned: continue

					#load pdbs
					if to_pdbc(sp.queryfn) not in pdbs:
						pdbc = to_pdbc(sp.queryfn)
						pdbs[pdbc] = superpose.load_pdb(sp.queryfn, prefix=pdbdir)
						pdbs[pdbc].blast = self.d2['b2a'][pdbc]
						pdbs[pdbc].load_derp('%s/derp/%s.derp' % (self.d2['p1d'], pdbc))
					if to_pdbc(sp.targetfn) not in pdbs:
						pdbc = to_pdbc(sp.targetfn)
						pdbs[pdbc] = superpose.load_pdb(sp.targetfn, prefix=pdbdir)
						pdbs[pdbc].blast = self.d2['b2a'][pdbc]
						pdbs[pdbc].load_derp('%s/derp/%s.derp' % (self.d2['p1d'], pdbc))

					sp.qpdbc = to_pdbc(sp.queryfn)
					sp.tpdbc = to_pdbc(sp.targetfn)
					sp.query = pdbs[to_pdbc(sp.queryfn)]
					sp.target = pdbs[to_pdbc(sp.targetfn)]

					sp.queryc = sp.query.chains[0]
					sp.targetc = sp.target.chains[0]

					sp.compute_aligned_tmss(threshold=tmsthreshold)


					fam1 = ''
					fam2 = ''

					qfam = sorted(pdbs[sp.qpdbc].blast)[0]
					for fam in self.d2['fams1']: 
						if qfam.startswith(fam): 
							fam1 = fam
					tfam = sorted(pdbs[sp.tpdbc].blast)[0]
					for fam in self.d2['fams2']: 
						if tfam.startswith(fam): 
							fam2 = fam

					try: self.alignments[fam1]
					except KeyError: self.alignments[fam1] = {}

					try: self.alignments[fam1][fam2]
					except KeyError: self.alignments[fam1][fam2] = []

					#if sp.qaligned and sp.taligned: self.alignments[fam1][fam2].append(sp)
					self.alignments[fam1][fam2].append(sp)


		#self.get_resolutions(spfns)




		#preprocess to collect PDBs
		if VERBOSITY: info('Collecting PDBs for %d alignments...' % n)

		#shuffle alignments off to the appropriate deck

		if VERBOSITY: info('Sending alignments to the appropriate containers')
		for fam1 in sorted(self.alignments.keys()): 
			for fam2 in sorted(self.alignments[fam1].keys()): 
				for alignment in sorted(self.alignments[fam1][fam2]):
					#if len(alignment.queryc) < len(alignment.targetc): cov = alignment.length/len(alignment.queryc)
					#else: cov = alignment.length/len(alignment.targetc)

					if is_alignment_good(alignment, lencutoff=lencutoff, tmsthreshold=tmsthreshold, bundlecov=bundlecov, mincov=mincov, worst_resolution=worst_resolution, allow_chimeras=allow_chimeras, maxrmsd=maxrmsd, d2data=self.d2):
						self.alngood[fam1][fam2].append(alignment)
					else:
						self.alnbad[fam1][fam2].append(alignment)


					if DEBUG: print(alignment.reason)

		if VERBOSITY: info('Writing summary...')

		if self.outfmt == 'tsv': self.summary_tsv(clobber=clobber)

	def summary_tsv(self, outfile='summary.tsv', maxbad=100, clobber=True):

		done = []
		for fam1 in sorted(self.d2['fams1']):
			for fam2 in sorted(self.d2['fams2']):
				if VERBOSITY: info('Generate summary for %s and %s?' % (fam1, fam2))
				if not self.alngood[fam1][fam2] and not self.alnbad[fam1][fam2]: 
					if VERBOSITY: info('Nope! Not good or bad!')
					continue
				if (fam1+fam2) in done: 
					if VERBOSITY: info('Nope! Already done!!')
					continue
				if (fam2+fam1) in done: 
					if VERBOSITY: info('Nope! DOne already!!')
					continue
				if VERBOSITY: info('Generating summary for %s and %s' % (fam1, fam2))

				done.append(fam1+fam2)
				done.append(fam2+fam1)

				if clobber: f = open('%s_vs_%s_%s' % (fam1, fam2, outfile), 'w') 
				else: f = open('%s_vs_%s_%s' % (fam1, fam2, outfile), 'a') 

				f.write('#==========================================================================\n')
				f.write('#                         %s vs %s\n' % (\
					re.sub('[^0-9A-Z]+$', '', fam1), \
					re.sub('[^0-9A-Z]+$', '', fam2)))
				f.write('#Ref\tCmp\tRefLen\tCmpLen\tRefTMS\tMovTMS\tRMSD\tAlnRes\tAlnCov\tAlgorithm\tAlnTMS\tAlnHelices\tReason\n')
				f.write('#==========================================================================\n')
				f.write('\n')

				if not self.alngood[fam1][fam2] and not self.alnbad[fam1][fam2]:
					f.write('No significant RMSD found\n' )

				else:
					f.write('### GOOD ###\n')
					if self.alngood[fam1][fam2]: 
						for alignment in sorted(self.alngood[fam1][fam2]): f.write(str(alignment) + '\n')
					else: f.write('No significant RMSD found\n')

					f.write('\n')

					f.write('### BAD ###\n')
					if self.alnbad[fam1][fam2]:
						for alignment in sorted(self.alnbad[fam1][fam2])[:maxbad]: f.write(str(alignment) + '\n')
						if len(self.alnbad[fam1][fam2]) > maxbad: 
							f.write('%d alignments not displayed\n' % (len(self.alnbad[fam1][fam2])-maxbad))
					else: f.write('No significant RMSD found\n')

				f.write('\n' * 2)

def generate_html(sps, p1dir='.', outdir='quomodo_out'):
	if not os.path.isdir('%s/pdbs_loopless' % p1dir): raise IOError('p1dir does not look like a valid Deuterocol1 path (Make sure $p1dir/pdbs_loopless exists)')

	if VERBOSITY: info('Doing one-time things')
	if not os.path.isdir(outdir): os.mkdir(outdir)
	if not os.path.isdir('%s/jsmol' % outdir): os.mkdir('%s/jsmol' % outdir)
	if not os.path.isdir('%s/pdbs' % outdir): os.mkdir('%s/pdbs' % outdir)
	style = ''
	style += 'td,th {border: 1px solid gray;margin: 0px; padding: 4pt;}\n'
	style += '.gray { color: gray; }\n'
	style += 'a.gray { color: #669; }\n'
	style += 'a { text-decoration: none; }\n'
	style += 'a:hover { text-decoration: underline; }\n'
	style += 'th { font-size: 70%; padding: 2pt; }\n'

	out = '<!DOCTYPE html>\n'
	out += '<html>\n'
	out += '<head>\n'
	out += '<meta charset="utf-8"/>\n'
	out += '<style>'
	out += style
	out += '</style>'
	out += '</head>\n'
	out += '<body>\n'
	out += '<h2>Table of family-family comparisons</h2>'
	out += '<table>\n'
	out += '<tr>\n'
	out += '<th>Families</th>\n'
	out += '<th>Closest alignment</th>\n'
	out += '<th>RMSD</th>\n'
	out += '<th>Length</th>\n'
	out += '<th>Longest alignment</th>\n'
	out += '<th>RMSD</th>\n'
	out += '<th>Length</th>\n'
	out += '</tr>\n'
	t0 = time.time()
	if VERBOSITY: info('Adding alignments to general table...')
	for label in sorted(sps): 
		out += '<tr>\n'

		gray = 0
		td = '<td>'
		if ' vs ' in label:
			sl = label.split(' vs ')
			if sl[0] == sl[1]: 
				gray = 1
				td = '<td class="gray">'


		out += td
		if not gray: out += '<a href="summary_%s.html">%s</a>' % (label.replace(' ', '_'), label)
		else: out += '<a class="gray" href="summary_%s.html">%s</a>' % (label.replace(' ', '_'), label)
		out += '</td>\n'
		lowest = None
		longest = None

		for sp in sps[label]:
			if lowest is None: lowest = sp
			elif sp.rmsd == lowest.rmsd:
				if sp.length > lowest.length: lowest = sp
			elif sp.rmsd < lowest.rmsd: lowest = sp

			if longest is None: longest = sp
			elif sp.length == longest.length:
				if sp.rmsd < longest.rmsd: longest = sp
			elif sp.length > longest.length: longest = sp

		out += td
		out += '%s vs. %s' % (lowest.qfull, lowest.tfull)
		out += '</td>\n'
		out += td
		out += '%0.2f' % lowest.rmsd
		out += '</td>\n'
		out += td
		out += '%d' % lowest.length
		out += '</td>\n'

		out += td
		out += '%s vs. %s' % (longest.qfull, longest.tfull)
		out += '</td>\n'
		out += td
		out += '%0.2f' % longest.rmsd
		out += '</td>\n'
		out += td
		out += '%d' % longest.length
		out += '</td>\n'

		out += '</tr>\n'
	out += '</table>\n'
	out += '</body>\n'
	out += '</html>\n'
	with open('%s/GENERALSUMMARY.html' % (outdir), 'w') as f: f.write(out)
	if VERBOSITY: info('Wrote general table (%0.2fs)' % (time.time() - t0))

	cpme = set()

	if VERBOSITY: info('Writing family tables...')
	t0 = time.time()
	t1 = time.time()
	for i, label in enumerate(sorted(sps)):
		out = '<!DOCTYPE html>\n'
		out += '<html>\n'
		out += '<head>\n'
		out += '<meta charset="utf-8"/>\n'
		out += '<style>\n'
		out += style
		out += '</style>\n'
		out += '</head>\n'
		out += '<body>\n'

		leftlink = ''
		uplink = '<a href="GENERALSUMMARY.html">&#x23F6; General summary</a>'
		rightlink = ''
		if i > 0: 
			other = sorted(sps)[i-1].replace(' ', '_')
			leftlink = '<a href="summary_%s.html">&#x23F4; %s</a> | ' % (other, other)
		elif i < (len(sps)-1):
			other = sorted(sps)[i+1].replace(' ', '_')
			rightlink = ' | <a href="summary_%s.html">%s &#x23F5;</a>' % (other, other)
		out += leftlink + uplink + rightlink + '<hr/>'

		out += '<h1>%s</h1>\n' % label
		out += '<table>\n'
		out += '<tr>\n'
		out += '<th>Alignment</th>'
		#out += '<th>OK</th>'
		out += '<th>RMSD (&Aring;)</th>'
		out += '<th>Length (aa)</th>'
		out += '<th>Min. cov. (%)</th>'
		out += '<th>A</th>'
		out += '<th>B</th>'
		out += '<th>C</th>'
		out += '<th>D</th>'
		out += '<th>Aln. B TMSs</th>'
		out += '<th>Aln. C TMSs</th>'
		out += '<th>B res. (&Aring;)</th>'
		out += '<th>C res. (&Aring;)</th>'
		out += '<th>Reason</th>'

		out += '</tr>\n'
		total = len(sps[label])
		if VERBOSITY: info('Adding %d entries to family table...' % total)
		for n, sp in enumerate(sorted(sps[label])):
			if VERBOSITY:
				if n in [int(total/100*5*x) for x in range(20)]: info('%d/%d added' % (n, total))
			cpme.add(sp.qpdbc)
			cpme.add(sp.tpdbc)
			out += '<tr id="%s_v_%s">\n' % (sp.qfull, sp.tfull)
			out += '<td><a href="%s_v_%s.html">%s vs. %s</a></td>\n' % (sp.qfull, sp.tfull, sp.qfull, sp.tfull)
			out += '<td>%0.2f</td>\n' % sp.rmsd
			out += '<td>%d</td>\n' % sp.length

			if sp.qpresent and sp.tpresent: out += '<td>%0.1f</td>\n' % (100*sp.length/min(sp.qpresent[-1]-sp.qpresent[0], sp.tpresent[-1]-sp.tpresent[0]))
			elif sp.qpresent: out += '<td>%0.1f</td>\n' % (100*sp.length/(sp.qpresent[-1]-sp.qpresent[0]))
			elif sp.tpresent: out += '<td>%0.1f</td>\n' % (100*sp.length/(sp.tpresent[-1]-sp.tpresent[0]))
			else: out += '<td>N/A</td>\n'

			#out += '<td>%d %d %d %d</td>\n' % (sp.qpresent[0], sp.qpresent[1], sp.tpresent[0], sp.tpresent[1])
			out += '<td></td>\n'
			out += '<td></td>\n'
			out += '<td></td>\n'
			out += '<td>%s</td>\n' % sp.reason
			out += '</tr>\n'

		out += '</table>\n'
		out += '</body>\n'
		out += '</html>\n'
		if VERBOSITY: 
			info('Wrote a table (%0.2fs)' % (time.time() - t1))
			t1 = time.time()
		with open('%s/summary_%s.html' % (outdir, label.replace(' ', '_')), 'w') as f: f.write(out)
	if VERBOSITY: info('Wrote family tables (%0.2fs)' % (time.time() - t0))

	if VERBOSITY: info('Copying PDBs to output directory...')
	for pdbc in sorted(cpme):
		shutil.copy('%s/pdbs_loopless/%s.pdb' % (p1dir, pdbc), '%s/pdbs/%s.pdb' % (outdir, pdbc))
	if VERBOSITY: info('Copied PDBs')

	n = 0
	t0 = time.time()
	for label in sps: 
		n += 1
		if VERBOSITY and (not (n % 10000)): 
			info('Generated %d summaries (%0.2fs)' % (n, time.time() - t0))
			t0 = time.time()
		for i, sp in enumerate(sorted(sps[label])):
			out = '<!DOCTYPE html>\n'
			out += '<html>\n<head>\n<meta charset="utf-8"/>\n<title>'
			out += 'QUOMODO summary of %s vs %s' % (sp.qfull, sp.tfull)
			out += '</title>\n<script type="text/javascript" src="jsmol/JSmol.min.js"></script>\n'
			out += '<script type="text/javascript">\n'

			script = '"set zoomlarge false; set antialiasDisplay; '
			script += "load append 'pdbs/%s.pdb'; " % sp.qpdbc
			script += "load append 'pdbs/%s.pdb'; " % sp.tpdbc
			script += 'rotate %s {1.1}; ' % sp.matrixstr

			if (sp.qpresent and sp.tpresent): hide_nonbundle = 'display {%d-%d /1.1 or %d-%d /2.1}; ' % (min(sp.qpresent), max(sp.qpresent), min(sp.tpresent), max(sp.tpresent))
			else: hide_nonbundle = ''

			show_nonbundle = 'display all; '
			script += hide_nonbundle

			if (sp.qpresent and sp.tpresent):
				script += 'center {%d-%d /1.1 or %d-%d /2.1}; ' % (min(sp.qpresent), max(sp.qpresent), min(sp.tpresent), max(sp.tpresent))

			script += 'cartoon only; model 0; zoom in; '

			#reset_colors = 'color {1.1} [xBB8888]; color {2.1} [x88BBBB]; '
			reset_colors = 'color {1.1} [xBBAA77]; color {2.1} [x77AABB]; '
			#if sp.qpresent: reset_colors += 'color {%d-%d /1.1} [xFFCCCC]; ' % (min(sp.qpresent), max(sp.qpresent))
			#if sp.tpresent: reset_colors += 'color {%d-%d /2.1} [xCCFFFF]; ' % (min(sp.tpresent), max(sp.tpresent))
			if sp.qpresent: reset_colors += 'color {%d-%d /1.1} [xFFDD99]; ' % (min(sp.qpresent), max(sp.qpresent))
			if sp.tpresent: reset_colors += 'color {%d-%d /2.1} [x99DDFF]; ' % (min(sp.tpresent), max(sp.tpresent))

			#for r in collapse(sp.qaligned): reset_colors += 'color {%d-%d /1.1} [xFF3333]; ' % tuple(r)
			#for r in collapse(sp.taligned): reset_colors += 'color {%d-%d /2.1} [x33FFFF]; ' % tuple(r)
			for r in collapse(sp.qaligned): reset_colors += 'color {%d-%d /1.1} [xFF9933]; ' % tuple(r)
			for r in collapse(sp.taligned): reset_colors += 'color {%d-%d /2.1} [x3399FF]; ' % tuple(r)

			script += reset_colors
			script += '"'
			out += '''\
	Jmol._isAsync = false;
	var JmolApplet0;
	var s = document.location.search;
	Jmol._debugCode = (s.indexOf("debugcode") >= 0);
	jmol_isReady = function(applet) {
		Jmol._getElement(applet,  "appletdiv").style.border = "1px solid gray";
	}
	var Info = {
		width: 800,
		height: 600,
		debug: false,
		color: "0xFFFFFF",
		use: "HTML5",
		j2sPath: "./jsmol/j2s",
		jarPath: "./jsmol/java",
		jarFile: "JmolAppletSigned.jar",
		isSigned: true,
		script: %s,
		readyFunction: jmol_isReady,
		disableJ2SLoadMonitor: false,
		disableInitialConsole: true,
		allowJavascript: true,
	};
	var cmdhistory = [];
	var histindex = 0;
	function jmol_runcmd() {
		if (histindex) {
			cmdhistory.pop();
			histindex = 0;
		}
		cmd = document.getElementById("cmd").value;
		if (cmd.length) {
			cmdhistory.push(cmd);
			Jmol.script(jmolApplet0, cmd);
		}
		document.getElementById("cmd").value = "";
	}
	function log_key(e) {
		if (cmdhistory.length == 0) return;
		else if (e.keyCode == KeyEvent.DOM_VK_UP) {
			if (histindex == 0) cmdhistory.push(document.getElementById("cmd").value);
			histindex = Math.min(cmdhistory.length - 1, histindex + 1);
			var current = cmdhistory.length-1-histindex;
			if (current >= 0 && current < cmdhistory.length) document.getElementById("cmd").value = cmdhistory[current];
		}
		else if (e.keyCode == KeyEvent.DOM_VK_DOWN) {
			if (histindex == 0) return;
			histindex = Math.max(0, histindex - 1);
			var current = cmdhistory.length-1-histindex;
			if (current >= 0 && current < cmdhistory.length) document.getElementById("cmd").value = cmdhistory[current];
			if (histindex == 0) cmdhistory.pop();
		}
	}

	$(document).ready(function() {
	$("#appdiv").html(Jmol.getAppletHtml("jmolApplet0", Info))
	})
	var lastPrompt = 0; ''' % script
			out += '</script>'
			out += '<style>body { font-family: sans-serif; } .monospace { font-family: monospace, Monospace, Lucida Sans Console, Monaco; font-size: 70%; } .scrollable {height: 40ex; overflow: scroll; resize: vertical; max-width: 500px;} #appdiv { float: left; } #respainter { float: left; } .jmol-container { width: 100%; } #jmolApplet0_appletinfotablediv { overflow: hidden; resize: both; }</style>\n'
			out += '</head>\n'
			out += '<body>\n'

			leftlink = ''
			uplink = '<a href="summary_%s.html">&#x23F6; %s summary</a>' % (label.replace(' ', '_'), label)
			rightlink = ''
			if i > 0: 
				other = sorted(sps[label])[i-1]
				leftlink = '<a href="%s_v_%s.html">&#x23F4; %s vs. %s (%0.2f &Aring;, %d aa)</a> | ' % (other.qfull, other.tfull, other.qfull, other.tfull, other.rmsd, other.length)
			elif i < (len(sps[label])-1):
				other = sorted(sps[label])[i+1]
				rightlink = ' | <a href="%s_v_%s.html">%s vs. %s (%0.2f &Aring;, %d aa) &#x23F5;</a>' % (other.qfull, other.tfull, other.qfull, other.tfull, other.rmsd, other.length)
			out += leftlink + uplink + rightlink + '<hr/>'

			out += '<h2>Red: %s (%s); Cyan: %s (%s)</h2><br/>\n' % (sp.qpdbc, sp.qfam, sp.tpdbc, sp.tfam)


			out += '<div class="jmol-container">\n'
			out += '<div class="appdiv-container">\n'
			out += '<div id="appdiv"></div>\n'
			out += '</div>\n'
			out += '<div id="respainter-container">\n'
			out += '<h3>Residue painter</h3>\n'
			out += 'Color: <input id="paintcolor" type="text" value="green" size="10"/><br/>'
			out += '<div id="respainter"><div class="scrollable"><table>\n'
			out += '(Out of order, sorry)\n'
			out += '</table>\n'
			out += '</div>\n<div style="clear: both;">\n'
			out += '<button onclick="Jmol.script(jmolApplet0, \'write image pngj 2 %s_v_%s.jmol;\');">Save this Jmol</button><br/>\n' % (sp.qfull, sp.tfull)
			out += '<button onclick="Jmol.script(jmolApplet0, \'%s\');">Reset colors</button><br/>\n' % reset_colors
			out += '<button onclick="Jmol.script(jmolApplet0, \'%s\');">Show nonbundle</button>\n' % show_nonbundle
			out += '<button onclick="Jmol.script(jmolApplet0, \'%s\');">Hide nonbundle</button><br/>\n' % hide_nonbundle
			out += '<form action="" method="post" onsubmit="jmol_runcmd(); return false;"><input size=80 id="cmd" onkeypress="log_key(event)"/></form>'
			out += '</div>'
			out += 'Quality: %0.2f<br/>\n' % sp.quality
			out += 'RMSD: %0.2f &Aring;<br/>\n' % sp.rmsd
			out += 'Alignment length: %d aa<br/>\n' % sp.length

			out += '</body>\n</html>'

			with open('%s/%s_v_%s.html' % (outdir, sp.qfull, sp.tfull), 'w') as f: f.write(out)

def parse_tsv(infile, p1dir='.'):
	'''
Parses TSV superposition tables

Use "#   some label like 1.A.1 vs 1.A.2" to label/partition sections
	'''
	sps = {}
	name = 'untitled superpositions'
	tcfams = ['Unassigned', 'Unassigned']

	pdb2tcid = {}
	tcid2fam = {}
	if VERBOSITY: info('Building conversion maps...')
	with open('%s/blastp.tbl' % p1dir) as f:
		for l in f:
			if l.startswith('#'): continue
			if not l.strip(): continue
			try: pdb2tcid[l.split()[0]]
			except KeyError: 
				pdb2tcid[l.split()[0]] = l.split()[1]
				tcid = l.split()[1].split('.')
				fam = '%s.%s.%s' % tuple(tcid[:3])
				tcid2fam[l.split()[1]] = fam

	if VERBOSITY: info('Parsing table...')
	with open(infile) as f:
		n = 0
		t0 = time.time()
		for l in f:
			n += 1
			if (not (n % 100000)) and VERBOSITY: 
				if DEBUG: 
					info('Parsed table to line %d (%0.2fs)' % (n, time.time() - t0))
					t0 = time.time()
				else: info('Parsed table to line %d' % n)
			if l.lstrip().startswith('#'): 
				if l[1:].startswith('   '): 
					name = l[1:].strip()
					if ' vs ' in name: tcfams = name.split(' vs ')
				else: continue
			elif not l.strip(): continue
			else: 
				sl = l.rstrip().split('\t')
				if len(sl) < 2: continue
				f = io.StringIO(sl[1])
				sp = superpose.Superpose(f=f, format='json', fn=sl[0])

				#sp.qfull = sl[0]
				#sp.tfull = sl[1]
				#expose loopless/looped here
				#sp.qfull = '%s/pdbs_loopless/%s.pdb' % (p1dir, sp.qpdbc)
				#sp.tfull = '%s/pdbs_loopless/%s.pdb' % (p1dir, sp.tpdbc)
				full = sl[0].split('_v_')
				sp.qfull = full[0]
				sp.tfull = full[1][:full[1].find('.')]

				#sp.qpdbc = sl[0][:6]
				#sp.tpdbc = sl[1][:6]
				qtms, ttms = {}, {}

				#load TMS assignments
				if len(sp.qpdbc) != 6: sp.qpdbc = sp.qfull[:6]
				if len(sp.tpdbc) != 6: sp.tpdbc = sp.tfull[:6]
				with open('%s/derp/%s.derp' % (p1dir, sp.qpdbc)) as g:
					for tms in g: 
						stms = tms.strip().split()
						qtms[int(stms[0])] = (int(stms[1]), int(stms[2]))

				with open('%s/derp/%s.derp' % (p1dir, sp.tpdbc)) as g:
					for tms in g:
						stms = tms.strip().split()
						ttms[int(stms[0])] = (int(stms[1]), int(stms[2]))

				#determine TMSs present based on names
				#TODO: automagically figure out somehow 

				#qr = [int(x) for x in sp.qfull[7:].split('-')]
				#sp.qpresent = list(range(\
				#	qtms[qr[0]][0], \
				#	qtms[qr[1]][1]))

				#tr = [int(x) for x in sp.tfull[7:].split('-')]
				#sp.tpresent = list(range(\
				#	ttms[tr[0]][0], \
				#	ttms[tr[1]][1]))

				##direct info
				##Ref\tCmp\tRefLen\tCmpLen\tRefTMS\tMovTMS\tRMSD\tAlnRes\tAlnCov\tAlgorithm\tAlnTMS\tAlnHelices
				if tcfams[0] == 'Unassigned': 
					sp.qfam = tcid2fam[pdb2tcid[sp.qpdbc]]
					#tcfams[0] = sp.qfam
				else: sp.qfam = tcfams[0]
				if tcfams[1] == 'Unassigned': 
					sp.tfam = tcid2fam[pdb2tcid[sp.tpdbc]]
					#tcfams[1] = sp.tfam
				else: sp.tfam = tcfams[1]

				#warn(name)
				#if name == 'untitled superpositions': name = '%s vs %s' % tuple(tcfams)
				#name = '%s vs %s' % tuple(tcfams)
				#warn(name + sp.qfam + sp.tfam)
				#warn(pdb2tcid['4qtn_A'] + pdb2tcid['4knf_A'])
				#warn(tcid2fam[pdb2tcid['4qtn_A']] + tcid2fam[pdb2tcid['4knf_A']])

				try: sps['%s vs %s' % (sp.qfam, sp.tfam)].append(sp)
				except KeyError: sps['%s vs %s' % (sp.qfam, sp.tfam)] = [sp]
	return sps

def get_matrices(sps, p2dir='.'):
	flatsps = []
	for k in sps: flatsps += sps[k]

	good = ''
	mode = 'tsv'
	for fn in os.listdir('%s/superpose' % p2dir): 
		if fn.endswith('tsvr'): mode = 'tsvr'
	for fn in os.listdir('%s/superpose' % p2dir): 
		if fn.endswith(mode): 
			with open('%s/superpose/%s' % (p2dir, fn)) as f: 
				for l in f:
					for i, sp in enumerate(flatsps):
						if l.startswith('%s_v_%s' % (sp.qfull, sp.tfull)): 
							good += l
							obj = json.loads(l.split('\t')[1])
							sp.matrixstr = obj['matrixstr']
							sp.quality = obj['quality']
							sp.qaligned = obj['qaligned']
							sp.taligned = obj['taligned']
							flatsps.pop(i)
							break
	if mode != 'tsvr':
		with open('%s/superpose/sp_good.tsvr' % (p2dir), 'w') as f:
			f.write(good)

def main(infile, p1dir='.', p2dir='.', outdir='quomodo_out', \
		lencutoff=50, \
		tmsthreshold=10, \
		bundlecov=0.8, \
		mincov=0.8, \
		worst_resolution=4., \
		allow_chimeras=False, \
		maxrmsd=5.0, \
		d2data=None \
	):

	if VERBOSITY: info('Parsing table...')
	t0 = time.time()
	sps = parse_tsv(infile, p1dir=p1dir)
	if VERBOSITY: info('Done in %0.1fs' % (time.time() - t0))

	pdbs = {}
	for fams in sps:
		for aln in sps[fams]:
			try: 
				aln.query = pdbs[aln.qpdbc]
			except KeyError: 
				pdbs[aln.qpdbc] = superpose.PDB('%s/pdbs_raw/%s.pdb' % (p1dir, aln.qpdbc[:4]))
				aln.query = pdbs[aln.qpdbc]
			try: 
				aln.target = pdbs[aln.qpdbc]
			except KeyError: 
				pdbs[aln.tpdbc] = superpose.PDB('%s/pdbs_raw/%s.pdb' % (p1dir, aln.tpdbc[:4]))
				aln.target = pdbs[aln.tpdbc]

	for fams in sps:
		removeme = []
		for i, aln in enumerate(sps[fams]):
			is_alignment_good(aln, lencutoff=lencutoff, tmsthreshold=tmsthreshold, bundlecov=bundlecov, mincov=mincov, worst_resolution=worst_resolution, allow_chimeras=allow_chimeras, maxrmsd=maxrmsd, d2data=d2data)

	if VERBOSITY: info('Collecting matrices...')
	t0 = time.time()
	get_matrices(sps, p2dir=p2dir)
	if VERBOSITY: info('Done in %0.1fs' % (time.time() - t0))

	if VERBOSITY: info('Generating HTML...')
	t0 = time.time()
	generate_html(sps, p1dir=p1dir, outdir=outdir)
	if VERBOSITY: info('Done in %0.1fs' % (time.time() - t0))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--p1d', help='Deuterocol1 directory')
	parser.add_argument('--p2d', help='Deuterocol2 directory')
	parser.add_argument('-o', '--outdir', default='quomodo_out', help='output directory')
	parser.add_argument('infile', nargs='+', help='TSV output to run on')

	args = parser.parse_args()

	if not os.path.isdir(args.p1d): raise IOError('Invalid directory: %s' % args.p1d)

	for fn in args.infile: 
		main(infile=fn, p1dir=args.p1d, p2dir=args.p2d, outdir=args.outdir)
