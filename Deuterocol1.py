#!/usr/bin/env python2
'''
This script collects PDBs corresponding to certain TCIDs (of any level) and assigns TMSs based on STRIDE and PDBTM (and a pinch of geometry)
'''
from __future__ import print_function, division
import xml.etree.ElementTree as ET
import os, re, subprocess, sys
import numpy as np
import yaml

import Bio.PDB

DEBUG = 1
VERBOSITY = 0
MAX_POSS_HELIX = 50

def status(line='done!'): 
	'''
prints 'done!' or anything else, really. Improves greppability when debugging with print statements
	'''
	print(line, file=sys.stderr)

def info(*lines):
	'''
prints INFO text to stderr
	'''
	for l in lines: print('[INFO]:', l, file=sys.stderr)

def warn(*lines):
	'''
prints WARNING text to stderr
	'''
	for l in lines: print('[WARNING]:', l, file=sys.stderr)

def error(*lines):
	'''
prints error text to stderr and exits
	'''
	for l in lines: print('[ERROR]:', l, file=sys.stderr)
	exit()

def progress(*lines):
	'''
prints INFO text to stderr without a trailing newline
	'''
	for l in lines: print('[INFO]:', l, end=' ', file=sys.stderr)

def prompt(line, default=None):
	'''
does y/n prompts
	'''
	while 1:
		if default is None:
			x = raw_input('%s [y/n] ' % str(line))
			if x.lower().strip().startswith('y'): return True
			elif x.lower().strip().startswith('n'): return False
		elif default is True:
			x = raw_input('%s [Y/n] ' % str(line))
			if x.lower().strip().startswith('n'): return False
			else: return True
		elif default is False:
			x = raw_input('%s [y/N] ' % str(line))
			if x.lower().strip().startswith('y'): return True
			else: return False

class Chain(object):
	'''
container for chain objects
	'''
	def __init__(self, id):
		self.id = id
		self.seq = ''
		self.tmh = []
		self.tms = []

class PDB:
	'''
container for PDB objects
	'''
	def __init__(self, fn): 
		self.chains = {}
		self.parse_xml(fn)

	def parse_xml(self, fn):
		'''
parse pdbtm entries built by pdbtmtop/dbtool.py (which cleans up the anomalous XML in places)
		'''
		self.tree = ET.parse(fn)
		self.root = self.tree.getroot()
		self.id = self.root.attrib['ID']
		###<BIOMATRIX> <APPLY_TO_CHAIN_CHAINID="A" NEW_CHAINID="D"> for example
		removeme = set()
		for x in self.root: 
			if x.tag.endswith('CHAIN'): 
				chainid = x.attrib['CHAINID']
				if x.attrib['TYPE'] == 'non_tm': continue
				for y in x:
					if y.tag.endswith('SEQ'): 
						seq = y.text.replace('U', 'X')
						seq = re.sub('[^\nA-Z]', '', seq).strip()
						if not seq.replace('X', '').strip(): break
						self.chains[chainid] = Chain(self.id + '_' + chainid)
						self.chains[chainid].seq = seq
					elif y.tag.endswith('REGION'):
						if y.attrib['type'] == 'H' or y.attrib['type'] == 'C': 
							self.chains[chainid].tmh.append((int(y.attrib['seq_beg'])-1, int(y.attrib['seq_end'])-1))
						if y.attrib['type'] == 'B': 
							self.chains[chainid].tms.append((int(y.attrib['seq_beg'])-1, int(y.attrib['seq_end'])-1))
					#print(dir(y))
			elif x.tag.endswith('BIOMATRIX'):
				for y in x:
					if y.tag.endswith('DELETE'): removeme.add(y.attrib['CHAINID'])
					elif y.tag.endswith('MATRIX'):
						for z in y:
							if z.tag.endswith('APPLY_TO_CHAIN'):
								removeme.add(z.attrib['NEW_CHAINID'])
		for c in list(removeme): 
			try: self.chains.pop(c)
			except KeyError: continue

	def cat(self): 
		'''
dumps the info
		'''
		out = ''
		for chain in self.chains: 
			out += '>%s\n%s\n' % (self.chains[chain].id, self.chains[chain].seq)
		return out.strip()

class BLAST:
	'''
container for blastp-related functions
	'''
	def __init__(self):
		self.hits = {}

	#def blast(self, query): pass
	#	#blastp -db tcdb -comp_based_stats no -outfmt 7 -max_target_seqs 3 < pdbtm.fa

	def parse7(self, results, minl=60, evalue=1e-5):
		'''
parser for outfmt 7. May be deprecated in favor of Biopython's BLAST results parser
		'''
		if type(results) is str: f = iter(results.split('\n'))
		else: f = results

		blacklist = ''
		for l in f:
			if not l.strip(): continue
			elif l.strip().startswith('#'): continue
			else: 
				sl = l.strip().split()
				if sl[0] == blacklist: continue

				data = (float(sl[2]),) + tuple([int(x) for x in sl[3:10]]) + (float(sl[10]), float(sl[11]))
				if data[1] < minl: continue
				elif data[8] > evalue: continue
				if data[0] >= 95: blacklist = sl[0]

				try: self.hits[sl[0]][sl[1]] = data
				except KeyError: self.hits[sl[0]] = {sl[1]:data}

		for q in self.hits:
			if len(self.hits[q]) > 1: 
				prods = sorted([(self.hits[q][t][0] * self.hits[q][t][1], t) for t in self.hits[q]])[::-1]
				self.hits[q] = {prods[0][1]:self.hits[q][prods[0][1]]}

	def by_target(self, namestart):
		'''
searches for all queries matching an initial substring of the target (i.e. targets matching /^${namestart}/)
		'''
		out = {}
		for q in self.hits:
			for t in self.hits[q]:
				if t.startswith(namestart): 
					try: out[q][t] = self.hits[q][t]
					except KeyError: out[q] = {t:self.hits[q][t]}
		return out

class Protocol1:
	'''
wrapper for doing its Protocol1-like task
	'''
	def __init__(self, pdbtmdir, outdir, force=False, offline=False):
		'''
sets up the workspace
		'''
		self.pdbs = []
		self.outdir = outdir
		self.pdbtmdir = pdbtmdir
		self.force = force
		self.helices = {}
		if not os.path.isdir(outdir): os.mkdir(outdir)
		self.ntmss = {}

		self.offline = offline
		self.blast = BLAST()
		self.hits = []

		self.lengths = {}
		self.fams = set()

		try:
			with open('%s/deuterocol1.yaml' % self.outdir) as f: 
				d1 = yaml.safe_load(f)
				self.lengths = d1['lengths']
				self.fams = set(d1['fams'])
		except IOError: pass

	def blast_pdbs(self):
		'''
blasts all PDBs in PDBTM against TCDB

TODO: optimize away identical sequences when possible, e.g. by collapsing them by BLASTing them against PDB to resolve to a single sequence and copying the results with modified query fields
		'''
		fastas = ''

		if self.offline: blasting = 0
		elif self.force: blasting = 1
		else:
			if os.path.isfile('%s/blastp.tbl' % self.outdir) and os.path.getsize('%s/blastp.tbl' % self.outdir): 
				#blasting = prompt('[WARNING]: Found an existing blastp table. Overwrite?', default=False)
				blasting = 0
			else: blasting = 1

			if VERBOSITY: progress('Checking PDBTM database...')

		pdb = ''
		for basename in os.listdir(self.pdbtmdir):
			if basename.lower().endswith('xml'): 
				self.pdbs.append(PDB(self.pdbtmdir + '/' + basename))
				seqs = self.pdbs[-1].cat().split('\n')
				for seg in seqs:
					if seg.startswith('>'): 
						pdb = seg[1:]
						self.lengths[pdb] = 0
					else: self.lengths[pdb] += len(seg)
		if VERBOSITY: status()
		self.dump_inputs()

		if blasting:
			for pdb in self.pdbs: 
				fastas += pdb.cat() + '\n'
			if VERBOSITY: progress('BLASTing %d sequences...' % len(self.pdbs))
			p = subprocess.Popen(['blastp', '-db', 'tcdb', '-comp_based_stats', 'no', '-outfmt', '7'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
			out, err = p.communicate(input=fastas)
			with open('%s/blastp.tbl' % self.outdir, 'w') as f: f.write(out)
			if VERBOSITY: status()
		else: 
			try:
				with open('%s/deuterocol1.yaml' % self.outdir) as f: self.lengths = yaml.safe_load(f)['lengths']
			except IOError: self.dump_inputs()
		with open('%s/blastp.tbl' % self.outdir) as f: self.blast.parse7(f)

	def get_queries(self, startswith): 
		'''
download the PDBs with decent correspondences to TCDB sequences

TODO (low): skip using wget by figuring out why urllib2 misbehaves with HTTPS URLs on the Macs
		'''
		self.fams.add(startswith)
		self.dump_inputs()
		for q in self.blast.by_target(startswith): 
			if q not in self.hits and not q.endswith('_'): 
				self.hits.append(q)

		#if self.force: write = 1
		#elif os.path.isfile('%s/pdblist.wget' % self.outdir): write = 0
		#else: write = 1
		write = 1

		pdbs = []
		for chain in self.hits: 
			if chain[:4] not in pdbs: pdbs.append(chain[:4])
		with open('%s/pdblist.wget' % self.outdir, 'w') as f: 
			for pdb in pdbs: 
				f.write('https://files.rcsb.org/view/%s.pdb\n' % pdb[:4])

		if not self.offline: subprocess.check_output(['wget', '--no-check-certificate', '-nc', '-i', '%s/pdblist.wget' % self.outdir, '-P', '%s/pdbs_raw' % self.outdir])
		#if offline:
		#	
		#	cache = os.listdir('%s/pdbs_raw' % self.outdir)
		#	missing = []
		#	for fn in cache:
		#		if fn.endswith('pdb') and not os.path.getsize('%s/pdbs_raw/%s' % (self.outdir, fn)): missing.append(fn)
		#	#if missing: raise IOError('Cannot run offline: Could not find %s' % missing)
		#	for pdb in pdbs: 
		#		if not os.path.getsize('%s/pdbs_raw/%s.pdb' % (self.outdir, pdb)): missing.append(pdb)
		#	#if missing: raise IOError('Cannot run offline: Could not find PDBs for %s' % missing)

		removeme = []
		for pdb in pdbs:
			with open('%s/pdbs_raw/%s.pdb' % (self.outdir, pdb)) as f:
				for l in f:
					if 'THEORETICAL MODEL' in l: 
						removeme.append(pdb)
						break
		removemefinal = []
		for pdb in removeme:
			for x in self.hits:
				if x.startswith(pdb): 
					removemefinal.append(x)
		for x in removemefinal: 
			self.hits.remove(x)
			
		return self.hits

	def assign_helices(self):
		'''
integrate STRIDE (assigns many small helices) and PDBTM (assigns correct but incomplete TMSs) definitions to get full TMSs
		'''
		if not os.path.isdir('%s/derp' % self.outdir): os.mkdir('%s/derp' % self.outdir)

		if VERBOSITY: progress('Computing helices...')
		removeme = []
		for pdb in self.hits:
			fn = '%s/pdbs_raw/%s.pdb' % (self.outdir, pdb[:4])
			try:
				d = DERP(pdb, self.outdir, self.pdbtmdir)
				self.helices[pdb] = d.get_helices()
				self.ntmss[pdb] = d.get_ntmss()
			except subprocess.CalledProcessError: removeme.append(pdb)
		for x in removeme: self.hits.remove(x)
			
		if VERBOSITY: status()
		self.dump_inputs()

	def generate_loopless(self, extend=2):
		'''
generate loopless PDBs (or loop-reduced PDBs, if extend is non-zero)
		'''
		for pdb in self.hits:
			out = ''
			chain = pdb[-1]
			with open('%s/pdbs_raw/%s.pdb' % (self.outdir, pdb[:4])) as f:
				for l in f:
					if l.startswith('DBREF'): 
						if l[11:13].strip() == chain: out += l
					elif l.startswith('SEQADV'):
						if l[15:17].strip() == chain: out += l
					elif l.startswith('SEQRES'):
						if l[10:12].strip() == chain: out += l
					elif l.startswith('MODRES'):
						if l[15:17].strip() == chain: out += l
					elif l.startswith('HET   '):
						if l[11:13].strip() == chain: out += l
					elif l.startswith('HELIX'):
						if l[18:20].strip() == chain: out += l
					elif l.startswith('SHEET'):
						if l[12:14].strip() == chain: out += l
					elif l.startswith('SSBOND'):
						if l[14:16].strip() == chain: out += l
						elif l[28:30].strip() == chain: out += l
					elif l.startswith('SITE  '):
						if l[10:12].strip() == chain: out += l
						elif l[21:23].strip() == chain: out += l
					elif l.startswith('CISPEP'):
						if l[14:16].strip() == chain: out += l
					elif l.startswith('LINK  '):
						if l[20:22].strip() == chain: out += l
						elif l[50:52].strip() == chain: out += l
					elif l.startswith('ANISOU'):
						continue#if l[20:22].strip() == chain: out += l
					elif (l.startswith('ATOM  ') or l.startswith('HETATM') or l.startswith('TER   ')) and (l[20:22].strip() == chain):
						for h in self.helices[pdb]:
							if (h[0] - extend) <= int(l[22:26].strip()) <= (h[1] + extend): 
								out += l
								break
					elif l[:6] not in ('DBREF ', 'SEQADV', 'SEQRES', 'HET   ', 'HELIX ', 'SHEET ', 'SSBOND', 'SITE  ', 'ATOM  ', 'HETATM', 'TER   ', 'CISPEP', 'ANISOU', 'LINK  ', 'MODRES'): 
						out += l
			if not os.path.isdir('%s/pdbs_loopless' % self.outdir): os.mkdir('%s/pdbs_loopless' % self.outdir)
			with open('%s/pdbs_loopless/%s.pdb' % (self.outdir, pdb), 'w') as f: f.write(out)

	def dump_inputs(self):
		'''
dump inputs to the deuterocol1 configuration file
		'''
		with open('%s/deuterocol1.yaml' % self.outdir, 'w') as f:
			yaml.safe_dump({'lengths':self.lengths, 'fams':list(self.fams), 'ntmss':self.ntmss}, f)

def parse_pdbtm(fn):
	'''
another PDBTM parser for some reason

TODO: remove one of them
	'''
	x = ET.parse(fn)
	root = x.getroot()
	helices = {}
	for y in root: 
		if y.tag.endswith('CHAIN'): 
			helices[y.attrib['CHAINID']] = []
			for z in y: 
				if z.tag.endswith('REGION') and (z.attrib['type'] == 'H' or z.attrib['type'] == 'C'): 
					#chainhelices[y.tag append
					helices[y.attrib['CHAINID']].append((int(z.attrib['pdb_beg']), int(z.attrib['pdb_end'])))

	return helices

class DERP:
	'''
Determine Egregious Rods in Proteins

This code does the actual integration between STRIDE and PDBTM
	'''
	def __init__(self, pdb_c, outdir, pdbtmdir):
		self.pdb_c = pdb_c
		self.pdb = pdb_c[:4]
		self.outdir = outdir
		self.pdbtmdir = pdbtmdir

	def get_tangent(self, c, interval):
		'''
averages the Can - Can+1 - Can+2 normals to obtain an axis angle for the segment

this works best with sufficiently long helices
		'''
		coords = []
		for model in self.structure:
			for chain in model:
				if chain.id == c:
					for residue in chain:
						if interval[0] <= residue.id[1] <= interval[1]:
							#print(dir(residue))
							coords.append([atom.coord for atom in list(residue.get_iterator())[:3]])
		normal = np.zeros(3)
		for i in range(1, len(coords)-1):
			for j in range(3):
				normal += np.cross(coords[i][j]-coords[i-1][j], coords[i+1][j]-coords[i][j])
		return normal/np.linalg.norm(normal)

	def get_ntmss(self):
		'''
gets the number of TMSs assigned to a PDB chain from DERP output
		'''
		n = ''
		n = 0
		with open('%s/derp/%s.derp' % (self.outdir, self.pdb_c)) as f:
			for l in f: 
				if not l.strip(): continue
				elif l.lstrip().startswith('#'): continue
				else: n += 1
				#n = l.strip().split()[0]
		return n

	def get_helices(self, angle=45):
		'''
attempts to get helix ranges from DERP output if possible and generates it if not
		'''
		if os.path.isfile('%s/derp/%s.derp' % (self.outdir, self.pdb_c)) and os.path.getsize('%s/derp/%s.derp' % (self.outdir, self.pdb_c)):
			helices = []
			with open('%s/derp/%s.derp' % (self.outdir, self.pdb_c)) as f:
				for l in f:
					if not l.strip(): continue
					else: helices.append([int(x) for x in l.split()[1:]])
			return helices

		elif not (os.path.isfile('%s/derp/%s.stride' % (self.outdir, self.pdb)) and os.path.getsize('%s/derp/%s.stride' % (self.outdir, self.pdb))):
			strideout = subprocess.check_output(['stride', '%s/pdbs_raw/%s.pdb' % (self.outdir, self.pdb)])
			with open('%s/derp/%s.stride' % (self.outdir, self.pdb), 'w') as f: f.write(strideout)

		stridehelices = {}

		parser = Bio.PDB.PDBParser()
		self.structure = parser.get_structure(self.pdb, '%s/pdbs_raw/%s.pdb' % (self.outdir, self.pdb))

		with open('%s/derp/%s.stride' % (self.outdir, self.pdb)) as f: 
			for l in f: 
				if l.startswith('LOC  AlphaHelix'): 
					chain = l[27:29].strip()
					start = int(l[21:27].strip())
					end = int(l[38:45].strip())
					try: stridehelices[chain].append((start,end))
					except KeyError: stridehelices[chain] = [(start,end)]

		pdbtmhelices = parse_pdbtm('%s/%s.xml' % (self.pdbtmdir, self.pdb))

		truetmhelices = []
		for ph in pdbtmhelices[self.pdb_c[-1]]:
			phcandidate = ph
			try:
				for sh in stridehelices[self.pdb_c[-1]]:
					if set(range(*sh)).intersection(set(range(*phcandidate))): 
						if sh[1] - sh[0] > MAX_POSS_HELIX: pass
						elif np.dot(self.get_tangent(self.pdb_c[-1], phcandidate), self.get_tangent(self.pdb_c[-1], sh)) > np.cos(angle*np.pi/180):
							phcandidate = (min(phcandidate[0], sh[0]), max(phcandidate[1], sh[1]))
			except KeyError: warn('Could not find chain %s of %s' % (self.pdb_c[-1], self.pdb))
			truetmhelices.append(phcandidate)
		#out = 'color red, i. '
		#for h in truetmhelices: out += '%s-%s+' % h
		#out = out[:-1]
		#out += '\ncolor yellow, i. '
		#for h in pdbtmhelices[self.pdb_c[-1]]: out += '%s-%s+' % h
		#out = out[:-1]
		#print(out)
		with open('%s/derp/%s.derp' % (self.outdir, self.pdb_c), 'w') as f:
			for i, h in enumerate(truetmhelices):
				f.write('%d\t%d\t%d\n' % ((i+1,)+h))
		return truetmhelices

def protocol1(fams, pdbtmdir='pdbtm', outdir='ubi_out', overwrite=False, extend=2):
	'''
the highest-level wrapper contained in Deuterocol1

does everything needed in a single line
	'''

	p = Protocol1(pdbtmdir, outdir, force=overwrite)
	p.blast_pdbs()
	for fam in fams: p.get_queries(fam)
	p.assign_helices()
	p.generate_loopless(extend=extend)

	relevant_hits = {}
	for pdb in p.hits: relevant_hits[pdb] = p.blast.hits[pdb]

	return p.hits, relevant_hits

if __name__ == '__main__':
	'''
finally, the interface for those running this directly from the command line or a non-Python script
	'''

	import argparse
	parser = argparse.ArgumentParser()

	parser.add_argument('-l', type=int, default=2, help='how many residues to extend TMSs by (for loopless cuts) {default:2}')
	parser.add_argument('-v', action='store_true', help='verbose output')
	parser.add_argument('-d', default='pdbtm', help='PDBTM database {default:./pdbtm}')
	parser.add_argument('-o', '--outdir', default='ubi_out', help='where to put everything')
	parser.add_argument('families', nargs='+', help='prefixes for family 1, e.g. 1.A.24.1. or 1.H.1. or 8.A.16')
	parser.add_argument('-F', '--force-overwrite', action='store_true', help='force overwrites/regenerations')

	args = parser.parse_args()

	if args.v: VERBOSITY = 1
	if not args.families: 
		print('[ERROR]: Family/ies must be specified!', file=sys.stderr)
		parser.print_usage()
		exit()

	protocol1(args.families, args.d, args.outdir, overwrite=args.force_overwrite, extend=args.l)
