#!/usr/bin/env python2
'''
Given Deuterocol1 output, superposes bundles of TMSs for appropriate structures for TCDB identifiers of any level and summarizes the best results
'''

from __future__ import print_function, division
import os, subprocess
import Deuterocol1
import quomodo
import yaml
import threading

VERBOSITY = 0

class Superpose(threading.Thread):
	'''
threading wrapper for running SSM Superpose
	'''
	def __init__(self, **kwargs):
		threading.Thread.__init__(self)
		self.cmdlist = kwargs['cmdlist']
	def run(self):
		for cmd in self.cmdlist:
			out = subprocess.check_output(cmd[0])
			with open(cmd[1], 'w') as f: f.write(out)

class Deuterocol2(object):
	'''
container for Deuterocol2 tasks
	'''
	def __init__(self, f1, f2, pdbtmd='pdbtm', p1d='ubi_out', p2d='ubi_out', bundle=0, cores=2):
		'''
sets up the workspace
		'''
		self.pdbs1, self.ab = Deuterocol1.protocol1(f1, pdbtmd, p1d, overwrite=False)
		self.pdbs2, self.cd = Deuterocol1.protocol1(f2, pdbtmd, p1d, overwrite=False)
		self.fams1 = f1
		self.fams2 = f2
		self.bundle = bundle
		self.cores = cores
		if p2d == 'auto':
			outname1 = ''
			for x in f1: outname1 += '_et_%s' % x
			outname1 += '_vs'
			outname2 = ''
			for x in f2: outname2 += '_et_%s' % x
			outname = outname1[4:] + outname2[3:]
			self.outdir = outname
		else: self.outdir = p2d

		if not os.path.isdir(self.outdir): os.mkdir(self.outdir)

		self.p1d = p1d

		self.dump_inputs()

	def dump_inputs(self):
		'''
dump inputs to the Deuterocol2 configuration file
		'''
		with open('%s/deuterocol2.yaml' % self.outdir, 'w') as f:
			full_hits = self.ab
			for k in self.cd: full_hits[k] = self.cd[k]
			yaml.safe_dump({'fams1':self.fams1, 'fams2':self.fams2, 'p1d':self.p1d, 'p2d':self.outdir, 'b2a':full_hits, 'bundle':self.bundle}, f)

	def calculate_bundle_indices(self):
		'''
computes the exact ranges of residues to be superposed
		'''
		self.bundles = {}
		self.ntmss = {}
		for pdb in list(set(self.pdbs1+self.pdbs2)):
			indices = []
			with open('%s/derp/%s.derp' % (self.p1d, pdb)) as f:
				for l in f: 
					if l.strip(): indices.append([int(x) for x in l.strip().split()[1:]])
					self.ntmss[pdb] = int(l.strip().split()[0])
			if self.bundle == 0: 
				try: self.bundles[pdb].append([indices[0][0], indices[-1][1]])
				except KeyError: self.bundles[pdb] = [[indices[0][0], indices[-1][1]]]
			elif len(indices) > self.bundle:
				for i in range(len(indices)-self.bundle+1):
					try: 
						self.bundles[pdb].append((indices[i][0], indices[i+self.bundle-1][1]))
					except KeyError: self.bundles[pdb] = [(indices[i][0], indices[i+self.bundle-1][1])]
			elif indices: self.bundles[pdb] = [(indices[0][0], indices[-1][1])]
			else: self.bundles[pdb] = []

	def count_superpose(self):
		'''
counts the number of superpositions to be done for the assigned job
		'''
		n = 0
		for pdb1 in self.pdbs1:
			for bundle1 in self.bundles[pdb1]:
				for pdb2 in self.pdbs2:
					for bundle2 in self.bundles[pdb2]:
						n += 1
		return n

	def superpose(self, looped=False, hence=False):
		'''
runs the superpositions to be done or provides the input for hence.py
		'''
		n = 0
		cmdlist = {}
		if not os.path.isdir('%s/superpose' % self.outdir): os.mkdir('%s/superpose' % self.outdir)

		skipped = 0
		if not hence:
			for pdb1 in self.pdbs1:
				if looped: fn1 = '%s/pdbs_raw/%s.pdb' % (self.p1d, pdb1[:4])
				else: fn1 = '%s/pdbs_loopless/%s.pdb' % (self.p1d, pdb1)

				for i1, bundle1 in enumerate(self.bundles[pdb1]):
					s1 = '%s/%d-%d' % (pdb1[-1], bundle1[0], bundle1[1])
					#bs1 = i1
					#for i in range(i1+1, i1+self.bundle): bs1 += '_%d' % i
					if self.bundle == 0: bs1 = '1-%s' % self.ntmss[pdb1]
					else: bs1 = '%s-%s' % (i1+1, i1+self.bundle)

					for pdb2 in self.pdbs2:
						if looped: fn2 = '%s/pdbs_raw/%s.pdb' % (self.p1d, pdb2[:4])
						else: fn2 = '%s/pdbs_loopless/%s.pdb' % (self.p1d, pdb2)

						for i2, bundle2 in enumerate(self.bundles[pdb2]):
							#bs2 = i2
							#for i in range(i2+1, i2+self.bundle): bs2 += '_%d' % i
							if self.bundle == 0: bs2 = '1-%s' % self.ntmss[pdb2]
							else: bs2 = '%s-%s' % (i2+1, i2+self.bundle)
							s2 = '%s/%d-%d' % (pdb2[-1], bundle2[0], bundle2[1])
							#out = subprocess.check_output(['superpose', fn1, '-s', s1, fn2, '-s', s2])
							#out = subprocess.check_output(['superpose', fn1, '-s', s1, fn2, '-s', s2])

							name = '%s_%s_v_%s_%s' % (pdb1, bs1, pdb2, bs2)

							if os.path.isfile('%s/superpose/%s.superpose' % (self.outdir, name)) and os.path.getsize('%s/superpose/%s.superpose' % (self.outdir, name)): 
								skipped += 1
								continue

							try: cmdlist[n].append((['superpose', fn1, '-s', s1, fn2, '-s', s2], '%s/superpose/%s.superpose' % (self.outdir, name)))
							except KeyError:
								cmdlist[n] = [(['superpose', fn1, '-s', s1, fn2, '-s', s2], '%s/superpose/%s.superpose' % (self.outdir, name))]
							n = (n + 1) % self.cores

							#with open('%s/superpose/%s_
							#print(pdb1, bundle1, pdb2, bundle2)
				if VERBOSITY: Deuterocol1.info('Skipped %d superpositions: Already done' % skipped)
			if cmdlist:
				if not hence:
					threads = []
					for i in range(self.cores):
						threads.append(Superpose(cmdlist=cmdlist[i]))
						threads[-1].start()

		else:
			cmdlist = {}
			for pdb1 in self.pdbs1:
				for i1, bundle1 in enumerate(self.bundles[pdb1]):
					s1 = '%s/%d-%d' % (pdb1[-1], bundle1[0], bundle1[1])
					if self.bundle == 0: bs1 = '1-%s' % self.ntmss[pdb1]
					else: bs1 = '%s-%s' % (i1+1, i1+self.bundle)
					for pdb2 in self.pdbs2:
						for i2, bundle2 in enumerate(self.bundles[pdb2]):
							if self.bundle == 0: bs2 = '1-%s' % self.ntmss[pdb2]
							else: bs2 = '%s-%s' % (i2+1, i2+self.bundle)
							s2 = '%s/%d-%d' % (pdb2[-1], bundle2[0], bundle2[1])
							try: cmdlist[n].append([pdb1, bs1, s1, pdb2, bs2, s2])
							except KeyError: cmdlist[n] = [[pdb1, bs1, s1, pdb2, bs2, s2]]
							n = (n + 1) % self.cores
			if cmdlist: 
				if not os.path.isdir('%s/hence' % self.outdir): os.mkdir('%s/hence' % self.outdir)
				for i in range(self.cores):
					with open('%s/hence/job_%d.hence' % (self.outdir, i), 'w') as f:
						f.write('# %s: %d/%d\n' % (self.outdir, i+1, self.cores))
						for line in cmdlist[i]: 
							cmd = ''
							for field in line: cmd += '%s\t' % (field)
							f.write(cmd.strip() + '\n')


def protocol2(f1, f2, pdbtmd='pdbtm', p1d='ubi_out', p2d='ubi_out', bundle=0, looped=False, cores=2, hence=False):
	'''
wrapper function for all Deuterocol2 functionality
	'''
	p = Deuterocol2(f1, f2, pdbtmd, p1d, p2d, bundle=bundle, cores=cores)
	p.calculate_bundle_indices()
	if VERBOSITY: Deuterocol1.progress('Performing %d superpositions...' % p.count_superpose())
	p.superpose(looped=looped, hence=hence)
	if VERBOSITY: Deuterocol1.status()
	return p

def summarize(p1d='ubi_out', p2d='ubi_out', outfile='summary.html', static=False, outfmt='tsv'):
	'''
Generates HTML report tables
	'''
	import yaml

	with open('%s/deuterocol2.yaml' % p2d) as f2: 
		with open('%s/deuterocol1.yaml' % p1d) as f1:
			#q = quomodo.Quomodo(yaml.safe_load(f1), yaml.safe_load(f2), outdir=p2d, outfile=outfile, outfmt=outfmt)
			q = quomodo.Quomodo(yaml.safe_load(f1), yaml.safe_load(f2), outdir=p2d, outfile=outfile, outfmt=outfmt)
	for i in range(0, len(q), 1e6):
		q.build_reports(outfile=outfile, nmin=i, nmax=i+1e6, clobber=(not i))

if __name__ == '__main__':
	'''
interface for those running Deuterocol2 from the shell or a non-Python script
	'''
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('-v', action='store_true', help='verbose output')
	parser.add_argument('-j', type=int, default=2, help='number of cores to incinerate^Wutilize {default:2}')

	parser.add_argument('-l', type=int, default=4, help='bundle size. 0 results in whole-structure alignments {default:4}')
	parser.add_argument('-o', action='store_true', help='keep loops')

	parser.add_argument('--pdbtmd', metavar='pdbtm_dir', default='pdbtm', help='PDBTM database location')
	parser.add_argument('--p1d', metavar='dir', default='ubi_out', help='where to store Deuterocol1 data')
	parser.add_argument('--p2d', metavar='dir', default='ubi_out', help='where to store Deuterocol2 data')

	parser.add_argument('--f1', metavar='families', nargs='+', help='first set of families')
	parser.add_argument('--f2', metavar='families', nargs='+', help='second set of families')

	parser.add_argument('--hence', action='store_true', help='do not perform the superpositions but instead store them to be run by hence')

	parser.add_argument('-f', metavar='format', default='tsv', help='output format. Implemented values are tsv and html')

	parser.add_argument('-k', action='store_true', help='skip running Deuterocol1+Deuterocol2 and just summarize existing results')


	args = parser.parse_args()

	if args.v: 
		VERBOSITY = 1
		Deuterocol1.VERBOSITY = 1
		quomodo.VERBOSITY = 1
	if not (args.f1 and args.f2): 
		parser.print_usage()
		exit()

	outfile = ''
	for fam in args.f1: outfile += '%s_' % fam
	outfile += '_vs_'
	for fam in args.f2: outfile += '%s_' % fam
	outfile = outfile[:-1]

	if not args.k: protocol2(args.f1, args.f2, args.pdbtmd, args.p1d, args.p2d, bundle=args.l, looped=args.o, cores=args.j, hence=args.hence)
	summarize(args.p1d, args.p2d, outfile=args.f)
